<!-- 
.. title: Display, viewport and screen resolution
.. slug: display-viewport-and-screen-resolution
.. date: 2017-05-22 21:08:51 UTC+01:00
.. tags: engine
.. category: 
.. link: 
.. description: Brief explanation of the differences between display, viewport and screen resolution
.. type: text
.. author: Al Glez
-->


Brief explanation of the differences between display viewport, viewport and screen resolution. This could be useful when you want to resize your game for different screens.

## Settings display viewport

It's the base resolution for your project, the one defined in the "project settings" (`scene > settings > display`), and since it's a fixed value, it's the one you need to focus when creating assets and resizing elements. 


```
Globals.get("display")
```

## Viewport 

The actual viewport size relative to the viewport defined in the settings, It's managed by the engine depending on the `strecth_mode`, `strecth_aspect` settings and the screen resolution. 

As an example if you are using some `stretch_mode` and `strecth_aspect = keep_width`, the width of the viewport will be always the width value defined in the settings no matter if the actual window or screen is bigger or smaller than that value.

```
get_viewport_rect().size
```

## Screen and window resolution

The screen or window resolution of the device you are running on.

```
OS.get_screen_size()

OS.get_window_size()
```

In mobile we can consider that the screen and window resolution will be equal since in most cases the engine will go fullscreen. 


## Resources and further reading

+ [Multiple Resolutions | godot documentation](http://docs.godotengine.org/en/stable/learning/features/viewports/multiple_resolutions.html)
