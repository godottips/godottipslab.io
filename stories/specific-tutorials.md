<!-- 
.. title: Specific tutorials
.. slug: specific-tutorials
.. date: 2017-07-08 15:26:05 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: 
-->

# Godot 2

## Godot specific tutorials ##

+ [Godot destructible terrain | fullstackend](https://www.fullstackend.com/2017/02/godot-destructible-terrain/)

## Code examples ##

+ [Godot 3d Water exmample | github](https://github.com/Alex-doc/Godot3DWaterExample)

### Shaders ###

+ [Godot shaders by HGRussian](https://github.com/HGRussian/godot_shaders)

## Videos ##

+ [Godot water 2d using shaders | Godojo](https://www.youtube.com/watch?v=1XcB46hN5Pg)

<!-- # Gdscript specific tutorials -->

<!-- # Game development specific tutorials -->

<!-- # Programming languages specific tutorials -->
