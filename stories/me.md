<!-- 
.. title: Me
.. slug: me
.. date: 2017-05-04 10:47:49 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Al Glez
-->

My name is Alberto. I'm probably a dinosaur. I drink a lot (really, a lot) of coffee.

I decided to start this blog as a place to share a lot of notes I took during my process of learning godot and developing some [stupid and simple game about colours](https://play.google.com/store/apps/details?id=org.godotengine.colours&hl=en).

You can [contribute](../how-to-collaborate) to this blog as well.

I'm trying to develop games as a freelance using godot (apart from working on other stuff)

I diversify my ego on the internet. I took very dark pictures of abandoned places or plain fog ([500px](https://500px.com/alglez), [flickr](https://www.flickr.com/photos/alglez), [instagram](https://www.instagram.com/migaveta/)), I listen to a lot of weird music ([spotify](https://open.spotify.com/user/algoban), [lastfm](https://www.last.fm/user/algoban)), I should code more ([gitlab](https://gitlab.com/algoban), [github](https://github.com/alglez)) and not be in many places at the same time ([tumblr](http://migaveta.tumblr.com/), [pinterest](https://es.pinterest.com/migaveta/), [ello](https://ello.co/alglez/)). You can contact me (al.glezbarreda@gmail.com, discord: algoban) or give me lots of money if you are part of some monarchy and with no descendants.
