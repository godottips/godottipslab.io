<!-- 
.. title: How to collaborate
.. slug: how-to-collaborate
.. date: 2017-05-04 10:48:11 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Al Glez
-->

You can collaborate sending articles to this blog. 

There aren't rules about what articles you can send, well that's not true, must be related somehow to godot (the game engine no that Samuel Becket book).

In order to publish you need a Gitlab Account [^1] and then just upload the file to posts (blog posts) or stories (pages).

This blog uses nikola as an static site generator, so the blog posts must be written in markdown or reStructuredText. You can use a local copy of nikola to generate those files [^2]. For example:

`nikola new_post -e -f markdown`

Alternatively, if you don't want to install nikola you can just use the header for those files. As for markdown:

``` markdown
<!-- 
.. title: 
.. slug: 
.. date: 
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author:
-->

```


[^1]: if you don't want to create one, you can simply send me the article to my mail al.glezbarreda@gmail.com 
[^2]: Read ([nikola documentation](https://getnikola.com/getting-started.html)) in order to install it and know the basics 
