<!-- 
.. title: Courses
.. slug: courses
.. date: 2017-10-17 17:48:10 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: 
-->

**Disclaimer**: There are several courses online for the godot engine. I can't endorse any of those as I have not enough information on any in term of quality or material. But, even though I have not taken part, the one by GDquest seems the only one really interesting of the ones in this list given the fact that all the free material by GDquest is quite good in terms of quality, design and general information. 

+ [Make 2d Games | Gdquest](https://gumroad.com/gdquest#]. Part of these course are this [30 days of godot tutorials][http://gdquest.com/tutorial/game-design/godot/30-days-free-game-creation-tutorial/)
+ [Godot Begginer to advanced | Udemy](https://www.udemy.com/godot-beginner-to-advanced-complete-course/)
+ [Android Game Development with Godot Engine | Udemy](https://www.udemy.com/godotengine/)
+ [Godot Engine - The complete course | Udemy](https://www.udemy.com/learngodot/)
