<!-- 
.. title: Resources
.. slug: resources
.. date: 2017-05-04 10:47:56 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author:
-->

There are other curated lists of resources by Andre Schmitz in [zeef](https://godot-engine.zeef.com/andre.antonio.schmitz) and by Calinou in [github](https://github.com/Calinou/awesome-godot)

There are major differences between versions of godot, you should have that in mind when looking tutorials or help pages for the godot engine. 

# Official
+ [Official Godot Web](https://godotengine.org/)
+ [Official Godot development blog](https://godotengine.org/devblog)
+ [Github project page](https://github.com/godotengine/godot)
+ [Official documentation](http://docs.godotengine.org/en/stable/)
+ [Godot twitter](https://twitter.com/godotengine)
+ [Patreon Page](https://www.patreon.com/godotengine)

# Communities
+ [Godot Q&A](https://godotengine.org/qa/)
+ [Godot facebook group](https://www.facebook.com/groups/godotengine)
+ [Godot community forum](https://godotdevelopers.org/forum/)
+ [Godot subreddit](https://www.reddit.com/r/godot/)
+ [Discord channel](http://discord.gg/zH7NUgz)

# Other resources lists in this blog
+ [List of example games with source code](../examples)
+ [List of specific tutorials](../specific-tutorials)
+ [List of modules for godot](../modules)
+ [Courses](../courses)
+ [Curated list of games made with godot](../games)

# Blogs
+ [Pixelated gears blog](http://fede0d.github.io/)
+ [Shinnil blog](http://shinnil.blogspot.com.es/)
+ [codetuto](http://codetuto.com/tag/godot/)
+ [IvanSkodje](http://ivanskodje.com/category/godot-engine/)

# Tutorials
## 2.+ series
+ [GDquest introduction to godot](http://gdquest.com/tutorial/game-design/godot/introduction-to-godot/)
+ [Erythrina tutorial](https://github.com/Jbat1Jumper/erythrina)
+ [Godot roguelike tutorial](https://razvanc-r.gitlab.io/tutorials/)

## Youtube Channels ##
### 2.+ Series ###

+ [Gamefromscratch godot series playlist](https://www.youtube.com/playlist?list=PLS9MbmO_ssyAXRl-_ktrebQBFxjSQt7UX).
+ [Splace Blaster 84 playlist tutorial](https://www.youtube.com/playlist?list=PLQHCSc6ohrIEQhXRljmCJuDZJHfujq-af)
+ [Andreas Esau godot tutorial series](https://www.youtube.com/playlist?list=PLPI26-KXCXpBtZGRJizz0cvU88nXB-G14)
+ [HeartBeast godot engine tutorials](https://www.youtube.com/playlist?list=PL9FzW-m48fn1iR6WL4mjXtGi8P4TaPIAp)
+ [Pixelated gears channel](https://www.youtube.com/channel/UCf1t2cnZEkUqDkDjvY6-NNw)
+ [Ivan Skodje youtube channel](https://www.youtube.com/channel/UCBHuFCVtZ9vVPkL2VxVHU8A)
+ [Pigdeve youtube channel](https://www.youtube.com/channel/UCFK9ZoVDqDgY6KGMcHEloFw)
+ [Godojo game development videos](https://www.youtube.com/user/JedSkates/videos)
+ [Kidscancode channel](https://www.youtube.com/channel/UCNaPQ5uLX5iIEHUCLmfAgKg/videos)

# Others

## Godot 3 builds
+ [Unofficial godot engine builds](http://fixnum.org/godot/)
+ [Godot 3 engine daily builds by Digitecnology](http://godot3builds.digitecnology.com/)
+ [Godot 3 builds by Calinou](https://godot.hugo.pro/)

## Performance tests
+ [Godot 3 Bunnymark](https://github.com/cart/godot3-bunnymark). As the project's page states "*Compares Godot 3.0 scripting languages by rendering as many bunny sprites as possible at 60fps*"

# Game Development and indie games resources
+ [Gamasutra](http://www.gamasutra.com/)
+ [Pixel Prospector](http://www.pixelprospector.com/)
+ [Gamedev reddit page](https://www.reddit.com/r/gamedev/)
+ [Book of shaders](http://thebookofshaders.com/)
