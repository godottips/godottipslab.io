<!-- 
.. title: Ignore mouse & stop mouse
.. slug: ignore-mouse-stop-mouse
.. date: 2017-10-13 20:17:59 UTC+01:00
.. tags: engine, 2d
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Al Glez
-->

In each `control` type node there are two properties quite interesting when dealing with overlapping objects that handle input events. 

A typical case scenario is when some `control` is not receiving events, in that case you should check that this two properties are set correctly in regards to the expected behaviour. Take into account that in godot the tree hierarchy works placing the object from bottom to top, meaning that the last object in the scene tree will be the last drawn or placed, and will overlap other objects.

## Ignore mouse or `focus\ignore_mouse` 

By default set to `false`.

If set to `true` It will ignore the mouse and pointer events (`input_event`) for the node in question, meaning that the node will not have any interaction with any `input_event`. This only affect the node in question, neither child or parent nodes.

For example, if you are using a `button` node with a signal when the button is `pressed()`, and ignore is set to `true` that node will not receive any event and as a result the signal will not be triggered. 

Consequently any other node under that control node will receive the signals.

## Stop mouse or `focus\stop_mouse`

By default set to `true`.

If set to `true` any parent node will not receive any event, meaning that every `input_event` will be absorbed by that node.

Three things to take into account:

* This does not affect to siblings, meaning that if a control overlap any sibling object this will not receive the signal no matter if `stop mouse` is set to `true` or `false`.
* This only affect to the place where those controls overlap.
* If `ignore mouse` is set to `true`, no matter how you configure `stop mouse` as it will not receive any event. 

## Behind parent or `visibility/behind_parent`

Another property available that influence how the events of overlapping objects are handled is the `behind parent` of tho CanvasItem class. 

This property alter the way are placed into the scene, meaning that if set to `true` the node will placed behind it parents, as a result this will alter which nodes will receive the events. 



## Resources and further reading

* [Class control | godot documentation](http://docs.godotengine.org/en/stable/classes/class_control.html)
