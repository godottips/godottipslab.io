<!-- 
.. title: List of Modules for godot engine
.. slug: modules
.. date: 2017-07-08 17:34:55 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: 
-->



# Godot 2 Modules

In the vast majority of the cases, to use this modules you need to compile godot with the module you want to use. You can read the documentation on [custom modules](http://docs.godotengine.org/en/stable/development/cpp/custom_modules_in_cpp.html), [creating android modules](http://docs.godotengine.org/en/latest/development/cpp/creating_android_modules.html) and [compiling godot](http://docs.godotengine.org/en/stable/development/compiling/) in the official documentation page and of course the documentation of the module you want to use. 

+ [Godot Unit Testing](https://github.com/bitwes/Gut). Utility to write tests for your game. 
+ [Script to build Godot export templates](https://gist.github.com/need12648430/d591fe3825fc4c3af5d7095e61e285fc)
+ [Godot  Themes](https://github.com/Geequlim/godot-themes). Includes syntax highlighting themes. 
+ [Godot syntax themes](https://github.com/Calinou/godot-syntax-themes) 
+ [Godot firebase integration for Godot](https://github.com/FrogSquare/GodotFireBase)
+ [GodotUI by FrogSquare](https://github.com/FrogSquare/GodotUI)

+ [GodotSql by FrogSquare](https://github.com/FrogSquare/GodotSQL). Module to access sql database

## Others

+ [Godot Assets by LeonardMeagher2](https://github.com/LeonardMeagher2/Godot-Assets)

## Social

+ [GodotGoogleService module by FrogSquare](https://github.com/FrogSquare/GodotGoogleService)
+ [Play Game Services module by ranmaru90](https://github.com/ranmaru90/Play-Game-Services-Android-Module-for-Godot)
+ [Godot facebook module by FrogSquare](https://github.com/FrogSquare/GDFacebook)
+ [bbGGPS by Teamblublee](https://github.com/teamblubee/bbGGPS). Google Play Services module
+ [GodotGps by Mavhod](https://github.com/Mavhod/GodotGPS). Google Play Services module

## Godot ads modules

+ [Godot ads by FrogSquare](https://github.com/FrogSquare/GodotAds)
+ [Adbuddiz by Alexey](https://bitbucket.org/reTTT/godotadbuddiz)
+ [GodotAppodeal by carlosmarti](https://github.com/carlosmarti/GodotAppodeal). Godot module for appodeal, there is available a [tutorial by shinnil](http://shinnil.blogspot.com.es/2015/12/quick-tutorial-how-to-use-appodeal-on.html) for this module.
+ [GodotHeyzap module by shin-nil](https://github.com/Shin-NiL/GodotHeyzap). Godot module for Heyzap. Check the [shinnil tutorial](http://shinnil.blogspot.com.es/2017/03/tutorial-using-heyzap-godot-module.html)
+ [bbAdmob by teamblublee](https://github.com/teamblubee/bbAdmob)
+ [Gomob by kamilors](https://github.com/kamilors/Gomob). Godot admob module for IOS
+ [GodotAdmob by Mavhod](https://github.com/Mavhod/GodotAdmob)

## Godot In App Purchase modules

+ GooglePaymantsV3 module is integrated in godot engine. [Read more](http://docs.godotengine.org/en/stable/learning/features/platform/android_in_app_purchases.html)
+ Servces for IOS is partially implemented in godot. [Read more](http://docs.godotengine.org/en/stable/learning/features/platform/services_for_ios.html)


## External tools

+ [Godot Tools for Visual Studio](https://marketplace.visualstudio.com/items?itemName=geequlim.godot-tools)
+ [Emacs major mode for Gdscript](https://github.com/brenttaylor/gdscript-mode)
+ [Gdscript for geany by haimat](https://github.com/haimat/GDScript-Geany)
+ [Gdscript for gedit by haimat](https://github.com/haimat/GDScript-gedit)
+ [Gdscript for atom by IndicaInkwell](https://atom.io/packages/lang-gdscript)
+ [Gdscript for vim by quabug](https://github.com/quabug/vim-gdscript)
+ [Gdscript support for sublimetext by beefsack](https://github.com/beefsack/GDScript-sublime)

# Godot 3

## Bindings for godot using GDnative

+ [Nim bindings](https://github.com/pragmagic/godot-nim)
+ [D language bindings](https://github.com/GodotNativeTools/godot-d)
+ [Mono integration with godot engine](https://github.com/neikeq/GodotSharp)
+ [Go language bindings](https://github.com/ShadowApex/godot-go)
