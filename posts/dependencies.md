<!-- 
.. title: Dependencies
.. slug: dependencies
.. date: 2017-07-06 18:08:52 UTC+01:00
.. tags: general, godot2, basic
.. category: 
.. link: 
.. description: 
.. type: text
.. author:  Al Glez
-->

 

To check the dependencies[^1] and the owners of a certain file (scene, script, sprite, etc.) and correct them you can `right_click` it within the **filesystem explorer** and select **Edit Dependencies...** or **View Owners...**. 

![filesystem explorer](/pics/dependencies_pic01.png)

If you select **Rename or Move...**, you can move or rename the resources and godot automatically will update all the explicit owners, so you don't need to update them manually. Bear in mind that this only applies to explicit owners, for example if you point to some location within your code, you will have to update that code manually. 

You can examine orphan resources (those without explicit owner) in `Tools > Orphan Resource Explorer `

![filesystem explorer](/pics/dependencies_pic02.png)

Godot will always warn you, when you open an scene, about missing explicit dependencies. 

All this is useful not only when you get errors, but when you are [restructuring your code](http://docs.godotengine.org/en/stable/learning/workflow/project_setup/project_organization.html) or to avoid wasting resources. 

[^1]: A dependency being a file needed by another file. For example the script used by a node, or an image used by an `Sprite` node, etc. 
