<!-- 
.. title: About
.. slug: about
.. date: 2017-05-04 10:48:17 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Al Glez
-->

Blog with some basic tips for the Godot game engine.

This is not a place intended as a tutorial for learning godot more as place with some basic tips, random notes and help material. You can look the [resources page](../resources) for other godot blogs, youtube channels, etc.

You can collaborate with this blog sending articles, check the information on [how to collaborate](../how-to-collaborate).

This blog is made with the [nikola static site generator](https://getnikola.com/) using gitlab pages. The base theme used is the [lanyon](https://github.com/poole/lanyon) modified version for nikola, with a lot of tweaks.

You can read about the guy (yes, me) behind this blog [here](../me).
