<!-- 
.. title: Privacy policy, play store and godot
.. slug: privacy-policy-play-store-and-godot
.. date: 2018-05-20 14:08:41 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Al Glez
-->


**Update:** [Fixing godot games published in google play | godot official blog](https://godotengine.org/article/fixing-godot-games-published-google-play)

Recently my [stupidgameaboutconnectingcoloredlines](https://play.google.com/store/apps/details?id=org.godotengine.colours&hl=en) was removed from Google Play because, as the kindly mail I receive stated, it violates play store's "personal and sensitive information policy". If you are a member of the [godot facebook page](https://www.facebook.com/groups/godotengine), you probably have seen that this had happen to many developers already. 

If you use any version prior to godot 2.1.5 or godot 3.0.x your app will be removed even if your app doesn't collect any data (as in my case), that's because godot use placeholder[^1]s within AndroidManifest.xml which can be potentially detected as permissions. There are differente approaches currently being discussed[^2] to fix this problem. 

So if you find yourself in this situation there are at least three solutions to the problem, use the more convenient one.

# Generate a privacy policy

This is the solution many have used. You have to link to your privacy policy directly from your app Play listing, you can place it in your personal webpage or any other accessible url.  

There are several privacy policy generators, as an example:

* [App Privacy Policy Generator](https://app-privacy-policy-generator.firebaseapp.com/)
* [freeprivacypolicy](https://www.freeprivacypolicy.com/)
* [Android Privacy Policy Template](https://gist.github.com/alphamu/c42f6c3fce530ca5e804e672fed70d78)

Many have opted to only link that privacy policy in the google Play listing and the app have been reaccepted, but reading through [google's policy](https://play.google.com/about/privacy-security-deception/personal-sensitive/) that shouldn't be enough as that privacy policy should be included in you app as well.[^3] 

# Use Apktool

[Apktool](https://ibotpeaches.github.io/Apktool/) is tool for reverse enigneering android apk files. 

In this case you need `android-sdk` and `apktool` installed.

1. Export your app through godot.
2. Decode your app with `apktool`[^4]
   
    ```sh
    apktool decode your_app.apk
    ``` 
    
3. Go to the new created folder and modify `AndroidManifest.xml` removing any unused permissions.
4. Build your app again with `apktool`[^5]. 
    
    ```sh
    apktool build your_app your_app_unaligned_unsigned.apk
    ```
    
5. Align and sign your app before submitting again.[^6]
    
    ```sh
    cd your_app/dist
    zipalign -v -p 4 your_app_unaligned_unsigned.apk your_app_unsigned.apk
    apksigner sign --ks your_key.keystore --out your_app.apk your_app_unsigned.apk
    ```
    
6. Release the new version and under store listing in the play console you should resubmit your app.

# Compile the templates

This is probably the least straight forward approach. 

You need to compile the android templates, but modifying `godot/platform/android/AndroidManifest.xml` and deleting the unused permissions before compiling the templates. 

I will not go into the details on how to compile templates as you can check the documentation on [building export templates](http://docs.godotengine.org/en/2.1/development/compiling/compiling_for_android.html).





[^1]: <https://github.com/godotengine/godot/issues/18192>
[^2]: <https://github.com/godotengine/godot/issues/18865>
[^3]: "Must be within the app itself, not only in the Play listing or a website" <https://play.google.com/about/privacy-security-deception/personal-sensitive/>
[^4]: <https://ibotpeaches.github.io/Apktool/documentation/#decoding>
[^5]: <https://ibotpeaches.github.io/Apktool/documentation/#building>
[^6]: <https://developer.android.com/studio/publish/app-signing#signing-manually>
